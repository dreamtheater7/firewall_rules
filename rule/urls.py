from django.urls import path
from . import views

# url configuration of the app rule
urlpatterns = [
    # empty string is the root of the app
    # rule/
    # rule/1/details
    path('', views.index, name='index')
]